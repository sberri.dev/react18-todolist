import PropTypes from "prop-types";
import TodoItem from "./TodoItem";
import './Todolist.css'

// Définition de la fonction TodoList
function TodoList(props) {
  const { todos } = props;

  // Création d'un tableau de composants TodoItem grâce à la méthode map et à partir de la liste de tâches
  const diplayToDoList = todos.map((todo) => (
    <TodoItem key={todo.id} title={todo.title} completed={todo.completed} />
  ))

  return (

    // dans cette div on retrouve le titre et la liste qui sont affichés dans le navigateur
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>{diplayToDoList}</ul> 
    </div>
  );
}

// Spécification des types de propriétés attendues par le composant TodoList
TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
